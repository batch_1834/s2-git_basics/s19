// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
let getCube = 2;

print = `The cube of ${getCube} is ${getCube**3}`
console.log(print)


// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

const address = ["258 Washington Ave NW", "Californiia 90011"]

let [Street, City] = address;

console.log(`I live at ${Street}, ${City}`);



// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.


const animal = {
	name: "Lolong",
	type: "Saltwater crocodile",
	weight: "1075 kgs",
	legnth: {
		feet: "20 ft",
		inches: "3 in"
	}
}

console.log(`${animal.name}is a ${animal.type}. He weighed at ${animal.weight} with a measurement of ${animal.legnth.feet} ${animal.legnth.inches}.`)

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const numbers =[1, 2, 3, 4, 5];

numbers.forEach(number => {
	console.log(number)
	
})
let reduceNumber = numbers.reduce((acc,current) => acc+current,0);console.log(reduceNumber);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
	
}

const myDog = new Dog ("Frankie", 5, "Miniature Dachshund");
console.log(myDog);


